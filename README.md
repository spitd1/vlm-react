_Základní stack pro vývoj projektu v Reactu_

Pro spuštění projektu je třeba mít nainstalované Node.js a npm

Hlavní příkazy
-

- `npm install` instalace balíčků do složky node_modules 
- `npm start` spustí projekt v prohlížeči na localhostu
- `npm run build` zkompiluje produkční verzi do složky dist

Další příkazy
-

- `npm run stage` spustí produkční verzi lokálně v prohlížeči 
- `npm run favicon` vygeneruje faviconu dle realfavicongenerator.net, před použitím je třeba globálně nainstalovat balíček cli-real-favicon `npm install -g cli-real-favicon`
- `npm run lint` zkontroluje JavaScript a vypíše chyby
- `npm run lint:autofix` opraví chyby v JavaScriptu
- `npm run lint:css` zkontroluje styly v souborech s příponou .scss 
- `npm run stats` zobrazí v prohlížeči velikost a strukturu produkčních souborů, před použitím je třeba nejdříve vygenerovat soubor stats.json pomocí příkazu `npm run build`

Struktura projektu
-

- **dist** - složka pro zkompilovanou produkční verzi
- **internals** - nastavení webpacku
- **src** - hlavní složka projektu, vstupní soubory index.html a index.js
  - *components* - hlavní komponenty
  - *data* - pomocná data
  - *static* - obrázky, fonty, ikony
  - *styles* - styly ve formátu SCSS 
  - *utils* - pomocné komponenty
