/* eslint-env node */
/* eslint-disable no-inline-comments */

const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const PostCSSAssetsPlugin = require('postcss-assets-webpack-plugin');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');

module.exports = options => ({
  entry: {
    main: './src/index.js'
  },
  output: {
    filename: '[name].[hash].js',
    chunkFilename: '[name].[hash].js',
    publicPath: '/',
    path: path.resolve(__dirname, '../../dist')
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.(ico|png|jpe?g|gif|svg|woff|woff2|ttf|eot|xml|webmanifest)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10000
            }
          }
        ]
      },
      {
        test: /\.scss$/,
        enforce: 'pre',
        loader: 'import-glob-loader'
      },
      {
        test: /\.s?css$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          {
            loader: 'postcss-loader',
            options: {
              ident: 'postcss',
              plugins: [
                autoprefixer({
                  cascade: false
                })
              ]
            }
          },
          'sass-loader'
        ]
      }
    ]
  },
  plugins: options.plugins.concat([
    new webpack.NamedModulesPlugin(),
    new MiniCssExtractPlugin({
      filename: '[name].[hash].css',
      allChunks: true,
      disable: process.env.NODE_ENV === 'development'
    }),
    new PostCSSAssetsPlugin({
      test: /\.css$/,
      plugins: [
        cssnano
      ]
    })
  ]),
  resolve: {
    modules: ['src', 'node_modules']
  },
  devtool: options.devtool,
  target: 'web',
  performance: options.performance || {},
  devServer: {
    contentBase: './dist',
    historyApiFallback: true
  },
  optimization: {
    splitChunks: {
      chunks: 'all',
      minChunks: 2
    }
  }
});
