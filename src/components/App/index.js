import React from 'react';

const App = () => (
  <h1>Hi, I am component. Rendered by React.</h1>
);

export default App;
